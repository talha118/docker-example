FROM vault:latest
FROM php:7.2-cli
COPY . /
WORKDIR /
COPY ./entrypoint.sh /
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]