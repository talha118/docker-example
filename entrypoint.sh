#!/bin/bash
handler(){
	echo 'RECEIVED\n'
	pkill -P $$
	exit
}
trap handler SIGKILL
php your-script.php &
php kill.php &
tail -f /etc/hosts